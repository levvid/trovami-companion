package com.mlginc.gibsonlevvid.trovamicompanion;

/**
 * Created by Gibson Levvid on 2/28/2017.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class CustomDeliveryLocationsList extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] locationName;
    private final String[] locationDescription;

    public CustomDeliveryLocationsList(Activity context,
                      String[] locationName, String[] locationDescription) {
        super(context, R.layout.activity_deliverylocations, locationName);
        this.context = context;
        this.locationName = locationName;
        this.locationDescription = locationDescription;


    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.activity_deliverylocations, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.delivery_location_name);


        txtTitle.setText(locationName[position]);


        return rowView;
    }
}

