package com.mlginc.gibsonlevvid.trovamicompanion;

/**
 * Created by Gibson Levvid on 8/2/2016.
 */


import com.backendless.Backendless;

public class Deliveries {
    private java.util.Date created;
    private String locationOfPackage;
    private Integer deliverStatus;
    private String objectId;
    private String ownerId;
    private String DeliveryPerson;
    private String DeliveryRecipient;
    private java.util.Date updated;

    public java.util.Date getCreated() {
        return this.created;
    }

    public String getLocationOfPackage() {
        return this.locationOfPackage;
    }

    public Integer getDeliverStatus() {
        return this.deliverStatus;
    }

    public String getObjectId() {
        return this.objectId;
    }

    public String getOwnerId() {
        return this.ownerId;
    }

    public String getDeliveryPerson() {
        return this.DeliveryPerson;
    }

    public String getDeliveryRecipient() {
        return this.DeliveryRecipient;
    }

    public java.util.Date getUpdated() {
        return this.updated;
    }


    public void setCreated(java.util.Date created) {
        this.created = created;
    }

    public void setLocationOfPackage(String locationOfPackage) {
        this.locationOfPackage = locationOfPackage;
    }

    public void setDeliverStatus(Integer deliverStatus) {
        this.deliverStatus = deliverStatus;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public void setDeliveryPerson(String DeliveryPerson) {
        this.DeliveryPerson = DeliveryPerson;
    }

    public void setDeliveryRecipient(String DeliveryRecipient) {
        this.DeliveryRecipient = DeliveryRecipient;
    }

    public void setUpdated(java.util.Date updated) {
        this.updated = updated;
    }

    public Deliveries save() {
        return Backendless.Data.of(Deliveries.class).save(this);
    }

    public Long remove() {
        return Backendless.Data.of(Deliveries.class).remove(this);
    }

    public static Deliveries findById(String id) {
        return Backendless.Data.of(Deliveries.class).findById(id);
    }

    public static Deliveries findFirst() {
        return Backendless.Data.of(Deliveries.class).findFirst();
    }

    public static Deliveries findLast() {
        return Backendless.Data.of(Deliveries.class).findLast();
    }
}