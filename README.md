# README #

### What is this repository for? ###

* Trovami Companion App. [Trovami Website](http://trovami.hostoi.com/)
* Version


### How do I get set up? ###

* Install the apk from app/app-release.apk or from the [Google Play Store](https://play.google.com/store/apps/details?id=com.mlginc.gibsonlevvid.prava)

### Contribution guidelines ###

* Contact the owner of the repo

### Who do I talk to? ###

* Gibson Levvid
* Trovami Inc.